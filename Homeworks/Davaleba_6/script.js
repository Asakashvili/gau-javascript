// #1
function transfer(element) { // <=== დავწერე ფუნქცია, დავარქვი სახელი "transfer" და ჩავუწერე ერთი პარამეტრი "element".
    document.getElementById("text1").value = element.value; // <=== 'document.getElementById'-ის მეშვეობით გამოვიძახე კონკრეტული 'id' 'html' ფაილიდან და ამ აიდის სახელწოდება ჩავუწერე მნიშვნელად.
// შემდეგ მივაბი '.value' ანუ მნიშვნელობა და გავუტოლე ელემენტის მნიშვნელობას. 
}


// #2 - Calculator
function buttoon(element) {
    document.getElementById("outcome").value += element
}

function solve() {
    let x = document.getElementById("outcome").value
    let y = eval(x)
    document.getElementById("outcome").value = y
}

function c() {
    document.getElementById("outcome").value = ""
}


// #3 - Simple Interest Calculator
function simpleInterest() {
    var principal = document.getElementById("principal").value
    var rate = document.getElementById("rate").value
    var period = document.getElementById("period").value
    var benefit = principal * rate * period / 100
    var total = principal + benefit;
    document.getElementById("benefit").innerHTML = benefit;
    document.getElementById("total").innerHTML = total;
}

function clr() {
    document.getElementById("principal").value = "" // <=== გავუტოლე სიცარიელეს რადგან წაშალოს კონკრეტული ტექსტი ღილაკზე დაჭერისას.
    document.getElementById("rate").value = ""
    document.getElementById("period").value = ""
}


// #4 - Compound Interest Calculator
function compoundInterest() {
    var principal1 = document.getElementById("principal1").value
    var rate1 = document.getElementById("rate1").value / 100
    var period1 = document.getElementById("period1").value
    var percentAmount = document.getElementById("percentAmount").value
    var total = principal1 * Math.pow((1 + rate1 / percentAmount), percentAmount * period1)
    var benefit = total - principal1;
    document.getElementById("benefit1").innerHTML = benefit;
    document.getElementById("total1").innerHTML = total;
}

function clr1() {
    document.getElementById("principal1").value = "" 
    document.getElementById("rate1").value = ""
    document.getElementById("period1").value = ""
    document.getElementById("percentAmount").value = ""
}


// #5 - Random Number Generator [0 - 30]
function randNumber() {
    document.getElementById("randNumber").innerHTML = Math.floor(Math.random() * 30);
}


// #6 - Random Number Generator [A - B]
function randomNumbers() {
    var a = document.getElementById("a").value
    var b = document.getElementById("b").value
    var random = Math.floor(Math.random() * (b - a + 1) + a)
    document.getElementById("generatenumber").innerHTML = random;
}

function clr2() {
    document.getElementById("a").value = "" 
    document.getElementById("b").value = ""
}


// #7
function randomImage() {
    img = ["1.jfif", "2.jfif", "3.jfif", "4.jfif", "5.jfif", "6.jfif", "7.jfif", "8.jfif", "9.jfif", "10.jfif"]
    r = Math.random() * img.length;
    rand = "Photos/" + img[Math.floor(r)];
    img = "<img class='img'" +rand+ "'></img>";

    return img;
}

function generateTable(row, column) {
    var body = document.getElementsByTagName("body")[0];
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");
    var row = document.getElementById("row").value
    var column = document.getElementById("column").value
    var img = document.getElementById("img").value

    for (var i = 0; i < row; i++) {
        var roww = document.createElement("tr");
        for (var j = 0; j < column; j++) {
            var cell = document.createElement("td");
            var cellText = document.createTextNode("Row: " +i+ " / Cell: " +j);
            cell.appendChild(cellText);
            roww.appendChild(cell);
        }
        tblBody.appendChild(roww);
    }

    for (var k = 0; k < img; k++) {
        var images = document.createElement("img")
        cell.appendChild(images);
    }

    images.setAttribute("src", "Photos/1.jfif")
    images.setAttribute("width", "60");
    images.setAttribute("height", "80");
    images.setAttribute("alt", "Photo");

    tbl.appendChild(tblBody);
    body.appendChild(tbl);

    tbl.setAttribute("width", "445")
    tbl.setAttribute("height", "450")
    tbl.setAttribute("border", "2")

    cell.setAttribute("border", "2")
    roww.setAttribute("border", "2")

    document.getElementById("randomTable").innerHTML = tbl;
}
