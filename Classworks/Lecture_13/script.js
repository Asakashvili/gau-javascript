function jQuery1(){
    var pes = $("input") // <=== წამოიღებს ყველა იმ თეგს რასაც '$' სიმბოლოს ჩავუწერთ.
    console.log(pes)
}

jQuery1()


// document.querySelector("#jq1").addEventListener("mouseover", function(){
//     console.log(this); // იგივეა რაც ქვემოთ. /JavaScript-ით/.
// })

$("#jq1").click(function(){ // <=== მოვნიშნეთ ეს ელემენტი ივენთის მისაბმელად. /jQuery-ით/.
    // console.log(this);
    $(".testDiv").hide(2000, function(){
        $(this).show(2000, function(){ // <=== ანიმაცია რომ დასრულდება 2 წამის შემდეგ ისევ რომ გამოვაჩინოთ.
            $(this).fadeOut(2000, function(){  // <=== 'fadeOut'-ის საშუალებით 'div'-ი რაღაც ანიმაციის დასრულების შემდეგ ნელ-ნელა ქრება.
                $(this).fadeIn(2000, function(){ // <=== 'fadeIn'-ის მეშვეობით გამქრალ დივს ისევ ვაჩენთ.
                   $(this).slideUp(2000, function (){ // <=== 'slideup'-ის მეშვეობით ზემოთ დაწერილი ანიმაციების დასრულების შემდეგ ეს 'div'-ი აიკეცება.
                      $(this).slideDown(2000) // <=== 'slideDown'-ის მეშვეობით ზემოთ დაწერილი ანიმაციების დასრულების შემდეგ ეს 'div'-ი ჩამოიკეცება.
                   }) 
                }) 
            }) 
        });
    });
})

$("#jq2").click(function(){ // <=== მოვნიშნეთ ეს ელემენტი ივენთის მისაბმელად. /jQuery-ით/.
    // console.log(this);
    $(".testDiv").toggle(2000); // <=== 'toggle'-ს შემთხვევაში ღილაკს მეორედ უნდა დავაჭიროთ კონკრეტული 'div'-ის გამოსაჩენად.
})

// ორი ივენთის მიბმა 'jQuery'-ს საშვალებით.
// $("jq1").on({
//     click:function (){
//         console.log("click")
//     },
//     mouseover:function (){
//         console.log("mouseover")
//     }
// })


function keypress_event(e){
    console.log("Key Press Event")
    console.log(e)
    console.log(e.charCode)
    console.log(e.keyCode)
}

function get_other_symbol(s, v){
    console.log(v)
    if(v.length!=0){
        console.log(v[v.length-1])
        var check_text = v[v.length-1]+s;
    }else{
        var check_text = ""
    }
    if(check_text=="br"){
        return "JAVASCRIPT"
    }else{
        return s
    }
}

function keydown_event(e){
    // console.log("Key Down Event")
    // console.log(e)
    // console.log("charCode "+e.charCode)
    // console.log("keyCode "+e.keyCode)
    // var record = document.getElementById("record")
    // var record1 = document.getElementById("record1")
    // record.value += get_other_symbol(e.key, e.target.value)
    // record1.innerText += get_other_symbol(e.key, e.target.value)
    if(e.key == "s"){
        st = setInterval(test, 1000)
    }
}

function keydown_event1(e){
    if(e.key == "f"){
        clearInterval(st)
    }
}

// 'let' = LOCAL; 'var' = GLOBAL, -within the function-; nothing = GLOBAL;
function testLet(a, b){
    if(a < b){
        let x = 13;
        var y = 14;
        z = 87;
        console.log("X="+x)
        console.log("Y="+y)
        console.log("Z="+z)
    }
    // console.log("X="+x)
    console.log("Y="+y)
    console.log("Z="+z)
}

function call(){
    testLet(3, 5)
    // console.log("Call"+x);
    // console.log("Call"+y);
    console.log("Call"+z);
}

// call()


function test(){
    var record = document.getElementById("record")
    record.value += "S"
}

function keyup_event(e){
    console.log("Key Up Event")
    console.log(e)
    console.log("charCode "+e.charCode)
    console.log("keyCode "+e.keyCode)
}