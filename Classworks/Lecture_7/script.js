d = document.querySelector("#d1") // <=== გარეთაა, ანუ განვსაზღვრეთ გლობალურად.
var html = ""

function click_event(){
    html += "<strong>Hello Click_event</strong><br>"
    d1_inner();
}

function over_event(){
    html += "<em>Hello over_event</em><br>"; // <=== '<br>' თეგით ჩამოდის ქვემოთ.
    d1_inner();
}

function up_event(){
    html += "<u>Hello up_event</u><br>"
}

function down_event(){
    html += "<span style='color: green'>Hello down_event<span><br>"
}

function d1_inner(){
    d.innerHTML = html;
}

function test1(){
    all_p = document.getElementsByTagName("p") // <=== 'HTML'-დან ყველა '<p>' თეგის წამოღება.
    console.log(all_p) // <=== დაიბეჭდება 'HTML' კოლექცია და მასში იქნება 5 '<p>' თეგი, ვინაიდან 'HTML'-ში გვიწერია 5 ცალი '<p>' თეგი.
    all_p[3].innerText = "<i>p4</i>"
    all_p[3].style.backgroundColor = "green"
    all_p[0].innerHTML = "<i>p0</i>"
}

// test1() 

function test2(){
    p = document.querySelector(".p1") // <=== 'querySelectorAll' იმუშავებს იგივენაირად როგორც 'getElementsByTagName'.
    console.log(p)
    d = document.querySelector("#d3")
    console.log(d)
}

function test3(){
    p = document.getElementById("p6") // <=== 'HTML'-ში აუცილებლად ვუწერთ შესაბამის იდენტიფიკატორს, წინააღმდეგ შემთხვევაში იქნება 'null'.
    // გამოიყენება იდენტიფიკატორის მოსანიშნად.
    p.style.backgroundColor = "yellow"
    p.innerText = "p6"
    console.log(p)
}

function test4(){
    p_class = document.getElementsByClassName("p")
    console.log(p_class)
    for(let i=0; i<p_class.length; i++){
        p_class[i].innerText = "Pclass - "+(i+1);
    }
}
