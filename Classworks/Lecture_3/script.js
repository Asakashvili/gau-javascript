console.log(Number.MAX_VALUE) // <=== ჯავასკრიპტში მაქსიმალური რიცხვი.
console.log(Number.MIN_VALUE) // <=== ჯავასკრიპტში მინიმალური რიცხვი.
x = 8
x = "8"
// x = "Giorgi"
console.log(isNaN(x)) // <=== თუ ცვლადი არის რიცხვი ეს დაგვიბრუნებს false (is not a number).
// როცა ვამოწმებთ ცვლადში არის თг არა რიცხვი^.
document.write(x+20)
if(isNaN(x)){
   // x = parseFloat(x);
   document.write("<p>X is not a number Hello "+x+"</p>")
}else{
   // x = parseFloat(x); // parseFloat-ით ჯამი მაინც დაიბეჭდება იმისდა მიუხედავად რომ x= "8" არ არის დაკომენტარებული.
   // parsefloat გამოიყენება ტიპის გარდაქმნისთვის.
   document.write("<p>"+(x+8)+"</p>")
}

console.log(Math) // <=== Math ობიექტი.
r =  Math.random() // <=== შემთხვევითი რიცხვი 0 - 1 შუალედში.
console.log(r) //<=== ^
r1 = r*10 // <=== შემთხვევით რიცხვი 0 - 10 შუალედში.
console.log(r1) // <=== ^
console.log(Math.round(r1)) // <=== დამრგვალება. 7.63 = 8.
console.log(Math.ceil(r1)) // <=== დამრგვალება მეტობით. 2.31 = 3. (ამაზე დიდი მთელი რიცხვი რაცაა).
console.log(Math.floor(r1)) // <=== დამრგვალება ნაკლებობით. 2.91 = 2 (ამაზე ნაკლები მთელი რიცხვი რაცაა).
console.log(r1.toFixed(2)) // <=== დამრგვალება, (2)-ით ეწერება ორი რიცხვი წერტილის შემდეგ.
console.log(r1.toPrecision(3)) 
console.log(Math.min(3, 4, 5, 56, 6, 6)) // <=== ჩამოთვლილი რიცხვებიდან გამოაქვს მინიმალური.
console.log(Math.max(3, 4, 5, 56, 6, 6)) // <=== ჩამოთვლილი რიცხვებიდან გამოაქვს მაქსიმალური.

function round_number(number, type){
   document.write("<hr>");
   if(type==1)
      document.write("<p>"+Math.round(number)+"</p>")
   
   if(type==2)
      document.write("<p>"+Math.ceil(number)+"</p>")

   if(type==3)
   document.write("<p>"+Math.floor(number)+"</p>")
}

round_number(87.987, 3)

function print_random_number(a, b){
   
}

