function start_game(){
    k = setInterval(add_ball, 100) // var არ დავუწერე რომ გლობალური იყოს.
}

function stop_game(){
    clearInterval(k)
}

function get_random_color(){
    let red = Math.floor(Math.random()*255)
    let blue = Math.floor(Math.random()*255)
    let green = Math.floor(Math.random()*255)
    return "rgb"+"("+red+","+blue+", "+green+")"
}

function add_ball(){
    var ball = document.createElement("div")
    ball.classList.add("ball")
    var square = document.querySelector(".square")
    // console.log(square)
    var width = parseInt(square.offsetWidth)-50
    // console.log(square.offsetWidth)
    ball.style.backgroundColor = get_random_color()
    var top = Math.floor(Math.random()*350)
    var left = Math.floor(Math.random()*width)
    ball.style.top = top+"px"
    ball.style.left = left+"px"
    var ball_count = document.querySelector("#ball-count")
    ball.addEventListener("click", removeBall)
    square.appendChild(ball)
    ball_count.innerText = parseInt(ball_count.innerText)+1
    if(parseInt(ball_count.innerText)==100){
        ball.removeEventListener("click", removeBall, true)
        clearInterval(k)
    }
    function removeBall(){
        var remove_ball_count = document.getElementById("remove-ball-count")
        remove_ball_count.innerText = parseInt(remove_ball_count.innerText)+1;
        ball_count.innerText = parseInt(ball_count.innerText)-1
        this.parentElement.removeChild(this)
    }
}

function create_element(el){
    var button = document.createElement("button");
    // var text = document.createTextNode("Hello World")
    // button.appendChild(text)
    button.innerText = "Hello World"
    button.style.backgroundColor = "green"
    button.style.marginRight = "10px"
    result1 = document.querySelector("#result1")
    result1.appendChild(button)
    console.log(button)
    button.addEventListener("click", function(){
        console.log(this)
        this.style.height = "3em"
    })
    // ჩამატება
    var p = document.createElement("p")
    p.innerText = "JavaScript"
    result2 = document.querySelector("#result2")
    console.log(result2.children) // <=== child elements (all)
    console.log(result2.querySelectorAll("p")) // <=== "p" tags only
    var p2 = result2.querySelectorAll("p")[1]
    result2.insertBefore(p, p2)
    console.log(p2)
    console.log(el)
    // el.setAttribute("disabled", "disabled") // disable button after a single click
    console.log(el.parentElement)
    var parent = el.parentElement;
    // parent.removeChild(el)
    setTimeout(function (){
        console.log(el)
        parent.removeChild(el)
    }, 3000)
    var anime = document.querySelector(".my-animation")
    animate(anime)
    // console.log(anime)
    // anime.style.animationName = "example"
    //  var id = setInterval(function (){
    //     anime.style.AnimationName = "example1"
    // }, 8000)
}

function animate(anime){
    var leftMarging = 0;
    var counter = 0;
    var id = setInterval(function (){
        leftMarging += 10
        anime.style.marginLeft = leftMarging+"px"
        counter ++;
        if(counter==20){
            clearInterval(id)
        }
    }, 100)
}

