var body = document.body
// console.log(body)
// console.log(body.children)
// console.log(body.childNodes)
// for(let i=0; i<body.children.length; i++){
//     // console.log(body.children[i])
//     if(body.children[i].children.length!=0){
//         console.log(body.children[i])
//         console.log("===========")
//         for (let j=0; j<body.children[i].children.length; j++){
//             console.log(body.children[i].children[j])
//         }
//     }
// }
//
// for(let i=0; i<body.childNodes.length; i++){
//     console.log(body.childNodes[i])
// }

var p2 = document.getElementById("p2")
console.log(p2)
console.log(p2.previousElementSibling) // <=== თუ გვაინტერესებს თუ რა არის 'p2'-ის წინა ელემენტი (ანუ 'HTML').
p2.nextElementSibling.innerHTML = "<a href='google.ge'>Google</a>" // <=== ჩავუმატეთ.
console.log(p2.nextElementSibling) // <=== თუ გვაინტერესებს თუ რა არის 'p2'-ის შემდეგი ელემენტი (ანუ 'სიცარიელე').
