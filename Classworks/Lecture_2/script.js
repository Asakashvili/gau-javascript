// სტრიქონები და მათი გაერთიანება
document.write("<h2>JavaScript Code is running</h2>")
x = "<h3>This is a variable</h3>"
document.write(x);
document.write("<hr>");
n = "Giorgi"
document.write("<p>Hello "+n+"</p>");
l = "Asakashvili"
info = "<p>Hello "+n+" my lastname is "+l+"</p>"
document.write(info)

// ცხრილის აგება JavaScript-ში და მასში HTML-ს გადაწერა
t = "<table class ='st-table'>" // <--- კლასი იწერება ერთმაგ ბრჭყალებში

t += "<tr>"
t += "<td>"
t += "Giorgi"
t += "</td>"
t += "<td>"
t += "</td>"
t += "</tr>"

t += "<tr>"
t += "<td>"
t += "</td>"
t += "<td>"
t += "</td>"
t += "</tr>"

t += "</table>" //td-ები უნდა ჩავწეროთ და ამიტომ გავაერთიანეთ ეს თეგები ამ გზით.
document.write(t)
console.log(t)

//ფუნქციები
function Task1_1(text){
    document.write("<p class='task-1'>"+text+"</p>"); 
    // p თეგების გარეშე ქვემოთ გამოძახებული ფუნქციები ერთ პარაგრაფში იქნებოდნენ.
}

Task1_1("Students");
Task1_1("Lectures");
Task1_1("Universities");

function Task1_3(text, size){
    document.write("<p style='font-size:"+size+"px'>"+text+"</p>")
}

Task1_3("JavaScript", 20);
Task1_3("JavaScript", 80);

function Task1_6(width, height){
   t = "<table class ='st-table' style='width:"+width+"px; height:"+height+"px'>"

   t += "<tr>"
   t += "<td>"
   t += "</td>"
   t += "<td>"
   t += "</td>"
   t += "</tr>"

   t += "<tr>"
   t += "<td>"
   t += "</td>"
   t += "<td>"
   t += "</td>"
   t += "</tr>"

   t += "</table>"
   document.write(t)
}

Task1_6(700, 300);
Task1_6(80, 30);

function Task1_6_1(width, height){
    t = "<table class ='st-table' style='width:"+width+"px; height:"+height+"px'>"
    
    for(i=0; i<5; i++){ //<--- for-ში გავამეორეთ. (ციკლის ოპერატორი) / Loop
        t += "<tr>";

        for(j=0; j<3; j++){ //<--- აქ სხვა ცვლადი დაგვჭირდა, მაგ: j.
            t += "<td>"
            t += "</td>"
        }
        
        t += "</tr>";

    }

    t += "</table>"
    document.write(t)
 }

 Task1_6_1(900, 400);