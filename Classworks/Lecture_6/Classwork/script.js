function f1(){
    // d = new Date();
    // console.log(d)
    console.log("Hello HTML "+s1) // <=== ეს სტრიქონი გამოვა თავისი იდენტიფიკატორით.
}

function f2(){
    console.log("Hello CSS "+s2)
}

// ცალკე ხერხი
var count = 1;
function f3(){
    console.log("Hello JavaScript "+s3)
    console.log("Count "+count)
    if (count==20){
        clearInterval(s3)
    }
    count++
}

// setTimeout(f1, 2000) // <=== 'setTimeout'-ის გამოსაყენებლად საჭიროა ფუნქციის დაწერა.
// 'setTimeout'-ში ვწერთ ფუნქციის სახელს და ვიძახებთ მას და ვუწერთ რამდენ ხანში მოხდეს ამ ფუნქციის გამოძახება, ამ შემთხვევაში 2 წამში.
// ფუნქციის გამოძახება ხდება მხოლოდ ერთხელ.

// setInterval(f1, 2000) // <=== 'setInterval' გამოიძახება რაღაც პერიოდულობით / ამ შემთხვევაში ყოველ ორ წამში ერთხელ.
// დრო ეწერება მილიწამებში.

// შესაძლებელია ამ 'setInterval'-ის გარკვეული დროის შემდეგ გაჩერება.
// ამისთვის, მის იდენტიფიკატორს ვწერთ ცვლადში.
var s1 = setInterval(f1, 1000)
// console.log(s1) // <=== გვიბრუნებს იმ რაოდენობას რამდენჯერაც ჩავწერეთ 'setInterval'.
// იდენტიფიკატორი შენახული გვაქვს 's'-ში.

var s2 = setInterval(f2, 1000)
// console.log(s2) // <=== გვიბრუნებს იმ რაოდენობას რამდენჯერაც ჩავწერეთ 'setInterval'.

var s3 = setInterval(f3, 1000)
// console.log(s3)

setTimeout(function(){ // <=== ანონიმური ფუნქცია.
    clearInterval(s1) // <=== 'clearInterval'-ის მეშვეობით ქვემოთ დაწერილი დროის შემდეგ შეწყვეტს 's2'-ის მნიშვნელობის გამოტანას.
    after3seconds(s2) // <=== გამოვიძახეთ ქვემოთ დაწერილი ფუნქცია რომელიც 's2' სტრიქონის გამოტანას შეაჩერებს 6 წამის შემდეგ, ანუ 's1' სტრიქონის შეჩერებიდან 3 წამის შემდეგ.
}, 3000) // <=== ანუ 3 წამის შემდეგ / მხოლოდ 3-ჯერ გამოიტანს.
// თავის ტანში აუქმებს მეორე 'setInterval'-ს.


function after3seconds(){
    setTimeout(function (){
        clearInterval(s2)
    }, 3000)
}