D = new Date(); // <=== დროითი ტიპი. შეგვიძლია ჩავუწეროთ მაგ: ("2015-03-25 00:00:00") და მაგ დროიდან აიწყებს ათვლას.
console.log(D);
console.log(D.getTime()) // <=== დრო მილიწამებში 1970 წლიდან დღემდე.
console.log(D.getHours())
console.log(D.getUTCHours())
console.log(D.getDay()) // <=== კვირის დღე.
console.log(D.getDate()) // <=== კვირიდან იწყებს ათვლას და ემთხვევა / აბრუნებს თარიღს, კვირის დღეს.
console.log(D.getMonth())
console.log(D.getFullYear()) // <=== გვაძლევს სრულ წელს.
console.log(D.getYear()) // <=== ეს 1900 წლიდან იწყებს ათვლას.